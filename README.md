## Dependencies:

* [docker](https://www.docker.com/)
* [docker-compose](https://docs.docker.com/compose/)
* [helm](https://helm.sh)
* [helmfile](https://github.com/roboll/helmfile)
* [kubectl](https://kubernetes.io/ru/docs/reference/kubectl/overview/)


## Installation:

### Helm
```bash
export HELM_VERSION=v3.3.4
wget https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz
tar zxvf helm-${HELM_VERSION}-linux-amd64.tar.gz
sudo mv ./linux-amd64/helm /usr/local/bin/
rm helm-${HELM_VERSION}-linux-amd64.tar.gz && rm -r ./linux-amd64
helm plugin install https://github.com/databus23/helm-diff --version v3.1.3
```
### Helmfile
``` bash
export HELMFILE_VERSION=v0.132.0
curl -LO "https://github.com/roboll/helmfile/releases/download/${HELMFILE_VERSION}/helmfile_linux_amd64"
sudo mv helmfile_linux_amd64 /usr/local/bin/helmfile
sudo chmod +x /usr/local/bin/helmfile
```
### Kubectl
``` bash
export KUBECTL_VERSION=v1.19.2
curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
sudo mv kubectl /usr/local/bin/kubectl
sudo chmod +x /usr/local/bin/kubectl
```

### Docker and docker-compose
``` bash
curl -L get.docker.com | bash

export DOCKER_COMPOSE_VERSION=1.27.4
curl -LO "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)"
sudo mv docker-compose-Linux-x86_64 /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### Create system monitoring directory
```bash
sudo mkdir -p /var/lib/grafana; sudo chown 472:472 /var/lib/grafana
sudo mkdir -p /var/lib/victoria-metrics
sudo mkdir -p /var/lib/alertmanager
```
### Running:
``` bash
git clone https://gitlab.com/denissa/kubernetes-monitoring.git
cd kubernetes-monitoring
docker-compose up -d
kubectl create ns observation
helmfile apply
```
